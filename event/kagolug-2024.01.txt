# 鹿児島Linux勉強会 2024.01(オンライン開催)

日時:: 2024/01/21(日) 14:00 〜 18:00 ( ~ )
会場:: online(self hosted Galene)
URL:: https://kagolug.connpass.com/event/307746/
参加者:: n人

## 前回

日時:: 2023-12-17(sun) 14:00-18:00 ( 〜 )
会場:: Self Hosted Galene https://www.kagolug.org:8443/
URL:: https://kagolug.connpass.com/event/300197/
参加者:: 6人

matoken , Nextcloud 28 upgrade で失敗, https://gitlab.com/matoken/kagolug-2023.12/-/blob/main/slide/slide.adoc

## 参加者

## 発表等

matoken,  X(old Twitter)の軽量フロントエンドのNitterをゲストアカウントで利用 https://gitlab.com/matoken/kagolug-2024.01/-/blob/main/slide/slide.adoc
kazueda「SSHアクセスログとかSDカードイメージとか」
yoheizuho「Mozcを入れるのに苦労してAnthyを入れた話」

## ネタ等

## 次回

次回 02/18(日)

## chat

鹿児島らぐミーティング
￼ Disable
Mute
Share Screen
kazueda13:58

よろしくお願いいたします。

yamano13:58

よろしくお願いします

matoken14:00

こんにちはー

yoheizuho14:03

よろしくお願いしますー

kazueda14:05

私の方では音が聞こえませんので、いったん抜けます

(anon)14:07

/help: display this help
/leave: leave group
/msg user message: send a private message
/presentfile: broadcast a video or audio file
/raise: raise hand
/renegotiate: renegotiate media streams
/sendfile user: send a file (this will disclose your IP address)
/unraise: unraise hand

matoken14:09

http://167.86.112.42:19001/p/kagolug-2024.01
matoken14:12

"nitter" https://nitter.net/
"zedeus/nitter: Alternative Twitter front-end" https://github.com/zedeus/nitter
yamano14:14

どちらも聞こえてます。

matoken14:15

"オープンソースカンファレンス2024 Osaka" https://event.ospn.jp/osc2024-osaka/
yamano14:26

SKKでしか生きれない体になりました。

yamano14:29

日本語の問題や、誤字脱字を気にせず押し通すおうになりましたw

yoheizuho14:32

すみません、言い忘れましたが今日は仕事の都合で１６時に失礼します

yamano14:51

88888

matoken15:05

"Fritter | F-Droid - Free and Open Source Android App Repository" https://f-droid.org/packages/com.jonjomckay.fritter/
matoken15:08

最近古いマシンしか使ってないのでドライバだいたい困らない……いいのか悪いのか

matoken15:10

あ，iconvで……

yamano15:17

88888

matoken15:38

"Home - Mind+ - Get ready to take the leap from zero to a coding expert" https://mindplus.cc/en.html
matoken15:40

"UNIHIKER - A single board computer brings you brand new experience." https://www.unihiker.com/
kazueda15:45

https://cloud5.jp/saitou-linux-automatic-logging/
matoken15:49

`/etc/ssh/sshd_config` の LogLevel

matoken15:51

       LogLevel
               Gives the verbosity level that is used when logging messages from sshd(8).  The possible values are: QUIET, FATAL, ERROR, INFO, VERBOSE, DEBUG, DEBUG1, DEBUG2, and  DEBUG3.   The  default  is
               INFO.  DEBUG and DEBUG1 are equivalent.  DEBUG2 and DEBUG3 each specify higher levels of debugging output.  Logging with a DEBUG level violates the privacy of users and is not recommended.
yoheizuho16:01

すみません、時間のため失礼します

matoken16:02

     -l log_level
             Specifies which messages will be logged by sftp-server.  The possible values are: QUIET, FATAL, ERROR, INFO, VERBOSE, DEBUG, DEBUG1, DEBUG2, and DEBUG3.  INFO and VERBOSE log transactions that
             sftp-server performs on behalf of the client.  DEBUG and DEBUG1 are equivalent.  DEBUG2 and DEBUG3 each specify higher levels of debugging output.  The default is ERROR.
matoken16:04

"電子書籍リーダー,4.2インチ,wifi付き2.9 1.54インチ,新品" https://ja.aliexpress.com/item/1005005086327690.html?srcSns=sns_Copy&spreadType=socialShare&bizType=ProductDetail&social_params=6000043691360&aff_fcid=10239948f26f4aa5a28c324e36705c83-1705820665148-09735-_olLq9au&tt=MG&aff_fsk=_olLq9au&aff_platform=default&sk=_olLq9au&aff_trace_key=10239948f26f4aa5a28c324e36705c83-1705820665148-09735-_olLq9au&shareId=6000043691360&businessType=ProductDetail&platform=AE&terminal_id=4552890b0aa9441599fb07a7eb30eb25&afSmartRedirect=y
kazueda16:07

https://btoshop.jp/products/b03079?_pos=1&_sid=edf81378f&_ss=r
kazueda16:11

https://github.com/Sarah-C/M5Stack_Paper_E-Book_Reader
https://www.youtube.com/watch?app=desktop&v=60MXwSN5l08
kazueda16:12

https://www.switch-science.com/products/7359
kazueda16:14

https://www.lilygo.cc/products/t5-4-7-inch-e-paper-v2-3
kazueda16:15

https://ja.aliexpress.com/item/1005002006058892.html
kazueda16:22

https://hwtechnical.com/rakuten-mini-disassembly/
matoken16:26

"Rakutenオリジナル修理料金表 | スマホ修理本舗" https://www.iphone-base.com/rakutenprice/
> Rakuten Mini
> キャンペーン中！        9,000円

kazueda16:30


Jan 21 16:29:34 raspi4 sftp-server[2042]: session opened for local user root from [192.168.0.31]
Jan 21 16:29:34 raspi4 sftp-server[2042]: received client version 3
Jan 21 16:29:34 raspi4 sftp-server[2042]: stat name "/root/mindplus/cache/test_rpi.mp"
Jan 21 16:29:34 raspi4 sftp-server[2042]: debug1: request 0: sent attrib have 0xf
Jan 21 16:29:34 raspi4 sftp-server[2042]: open "/root/mindplus/cache/test_rpi.mp/.cache-file.py" flags WRITE,CREATE,TRUNCATE mode 0666
Jan 21 16:29:34 raspi4 sftp-server[2042]: debug1: request 1: sent handle handle 0
Jan 21 16:29:34 raspi4 sftp-server[2042]: debug1: request 2: fsetstat handle 0
Jan 21 16:29:34 raspi4 sftp-server[2042]: set "/root/mindplus/cache/test_rpi.mp/.cache-file.py" mode 0666
Jan 21 16:29:34 raspi4 sftp-server[2042]: sent status Success
Jan 21 16:29:34 raspi4 sftp-server[2042]: debug1: request 3: write "/root/mindplus/cache/test_rpi.mp/.cache-file.py" (handle 0) off 0 len 86
Jan 21 16:29:34 raspi4 sftp-server[2042]: sent status Success
Jan 21 16:29:34 raspi4 sftp-server[2042]: close "/root/mindplus/cache/test_rpi.mp/.cache-file.py" bytes read 0 written 86
Jan 21 16:29:34 raspi4 sftp-server[2042]: sent status Success
Jan 21 16:29:34 raspi4 sftp-server[2043]: session opened for local user root from [192.168.0.31]
Jan 21 16:29:34 raspi4 sftp-server[2043]: received client version 3
Jan 21 16:29:34 raspi4 sftp-server[2043]: open "/usr/local/lib/python3.7/dist-packages/.pth" flags WRITE,CREATE,TRUNCATE mode 0666
Jan 21 16:29:34 raspi4 sftp-server[2043]: sent status No such file
Jan 21 16:29:34 raspi4 sftp-server[2043]: debug1: read eof
Jan 21 16:29:34 raspi4 sftp-server[2043]: session closed for local user root from [192.168.0.31]

￼matoken16:59
値段期にしなかったら欲しいやつ
Android/GooglePlay対応/E-Ink
"Amazon.co.jp: BOOX Poke5 ６インチAndroid11搭載 電子書籍リーダー : パソコン・周辺機器" https://www.amazon.co.jp/dp/B0C5J7MWX9

matoken17:01
"Amazon.co.jp: SKTSELECT BOOX Palma 6.13インチHDモバイル電子ペーパータブレット Eink Carta1200 Android11 GooglePlay クアルコム8コア BSR搭載 RAM6GB ROM128GB MicroSDXC(最大2TB) Wifi及びBT利用可能 ページめくり及びカスタム物理ボタン付 カメラとスピーカー付 170g 黒 : パソコン・周辺機器" https://www.amazon.co.jp/dp/B0CJ86XG66

matoken17:05
"turgu1/EPub-InkPlate: An EPub Reader for the ESP32 based InkPlate e-Ink devices." https://github.com/turgu1/EPub-InkPlate

matoken17:07
"vroland/epdiy: EPDiy is a driver board for affordable e-Paper (or E-ink) displays." https://github.com/vroland/epdiy

yamano17:20
https://ja.wikipedia.org/wiki/%E9%96%A2%E6%88%B8%E6%A9%8B%E3%83%95%E3%83%AA%E3%83%BC%E3%83%9E%E3%83%BC%E3%82%B1%E3%83%83%E3%83%88

https://ameblo.jp/cicli-la-bellezza/entry-12825440803.html

matoken17:50
https://ef69-150-66-116-220.ngrok-free.app/

"ttyd でSixel – matoken's meme" https://matoken.org/blog/2024/01/05/sixel-with-ttyd/

matoken17:56
次回 02/18(日)

kazueda17:56
2/18了解です。

yamano18:01
おつかれさまでした~~~

kazueda18:01
ありがとうございました。

Tokyoですか？

spring

matoken18:02
"オープンソースカンファレンス2024 Online Spring" https://event.ospn.jp/osc2024-online-spring/

kazueda18:03
かごらぐでセッションを申し込みのですか？

kazueda18:05
お疲れさまでした。

yamano18:05
おつかれさまでした

matoken18:05
👋
